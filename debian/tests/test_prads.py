import re

PCAP="fixtures/test.pcap"
LOG="fixtures/test.asset.log"

def test_package(host):
    prads = host.package('prads')
    assert prads.is_installed

def test_service(host):
    prads = host.service('prads')
    assert prads.is_running
    assert prads.is_enabled

def test_run_prads(host):
    cmd = host.run("prads -r %s -l %s" % (PCAP, LOG))
    assert cmd.rc == 0
    assert re.search(r"Total packets received from libpcap\s+:\s+18$", cmd.stdout, flags=re.MULTILINE)
    assert re.search(r"Total TCP OS fingerprints detected\s+:\s+4$", cmd.stdout, flags=re.MULTILINE)

def test_run_prads_asset_report(host):
    cmd = host.run("prads-asset-report -n -r %s", LOG)
    assert cmd.rc == 0
    assert re.search(r"80\s+CLIENT\s+Debian\sAPT", cmd.stdout)
    assert re.search(r"53\s+SERVER\s+DNS\sSQR\sNo\sError", cmd.stdout)
